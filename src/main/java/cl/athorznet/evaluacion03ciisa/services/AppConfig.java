package cl.athorznet.evaluacion03ciisa.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class AppConfig extends Application{
    
}
