package cl.athorznet.evaluacion03ciisa.services;

import cl.athorznet.evaluacion03ciisa.model.dao.TbEquipoDao;
import cl.athorznet.evaluacion03ciisa.model.dao.exceptions.NonexistentEntityException;
import cl.athorznet.evaluacion03ciisa.persistence.entities.TbEquipo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/equipos")
public class EquiposRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("evaluacion03_heroku_PU");
    EntityManager em;
    TbEquipoDao dao = new TbEquipoDao();
    //Lista de todos los equipos registrados

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarEquiposAll() {

        System.out.println("Obteniendo Lista desde DAO");
        List<TbEquipo> lista = dao.findTbEquipoEntities();

        return Response.ok(200).entity(lista).build();
    }

    //Busca elementos de acuerdo al ID, en este caso el PK de tipo Integer
    @GET
    @Path("/{buscarid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("buscarid") Integer buscarid) {

        System.out.println("Buscando ID desde DAO");
        TbEquipo equipo = dao.findTbEquipo(buscarid);

        return Response.ok(200).entity(equipo).build();
    }

    //Inserta un nuevo elemento en la Base de Datos
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevoReg(TbEquipo equipoNuevo) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(equipoNuevo);
        em.getTransaction().commit();

        return "Equipo nuevo Registrado!";
    }

    
    //Modifica un elemento existente en la BD (Se deberá ingresar el ID de registro en el JSON)
    @PUT
    public Response actualizaReg(TbEquipo equipoUpdate) throws Exception {
        TbEquipo equi = equipoUpdate;
        dao.edit(equi);
        
        return Response.ok(equipoUpdate).build();
    }
    //Modifica un elemento existente en la BD, Pero esta vez, ingresando el ID de registro en la URI
    @PUT
    @Path("/{updateid}")
    public Response actualizaReg(@PathParam("updateid") Integer updateid, TbEquipo equipoUpdate) throws Exception {

        if (dao.findTbEquipo(updateid) != null) {
            TbEquipo equi = equipoUpdate;
            equi.setIdRegistro(updateid);
            dao.edit(equi);

            return Response.ok(equipoUpdate).build();
            
        } else {
            return Response.ok("Equipo ID: " + updateid + " NO EXISTE!!").build();
        }

    }

    //Elimina un registro de la BD
    @DELETE
    @Path("/{deleteid}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminarId(@PathParam("deleteid") Integer deleteid) {

        TbEquipo equipoDel = dao.getEntityManager().getReference(TbEquipo.class, deleteid);

        try {
            dao.destroy(deleteid);

        } catch (NonexistentEntityException ex) {
            Logger.getLogger(EquiposRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Equipo Eliminado!!\n Detalle Registro:" + equipoDel.toString()).build();
    }
}
