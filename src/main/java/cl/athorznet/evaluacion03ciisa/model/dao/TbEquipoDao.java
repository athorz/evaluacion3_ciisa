/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacion03ciisa.model.dao;

import cl.athorznet.evaluacion03ciisa.model.dao.exceptions.NonexistentEntityException;
import cl.athorznet.evaluacion03ciisa.persistence.entities.TbEquipo;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jarqu
 */
public class TbEquipoDao implements Serializable {

    public TbEquipoDao() {
    }

    public TbEquipoDao(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("evaluacion03_heroku_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TbEquipo tbEquipo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tbEquipo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TbEquipo tbEquipo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tbEquipo = em.merge(tbEquipo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tbEquipo.getIdRegistro();
                if (findTbEquipo(id) == null) {
                    throw new NonexistentEntityException("The tbEquipo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TbEquipo tbEquipo;
            try {
                tbEquipo = em.getReference(TbEquipo.class, id);
                tbEquipo.getIdRegistro();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tbEquipo with id " + id + " no longer exists.", enfe);
            }
            em.remove(tbEquipo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TbEquipo> findTbEquipoEntities() {
        return findTbEquipoEntities(true, -1, -1);
    }

    public List<TbEquipo> findTbEquipoEntities(int maxResults, int firstResult) {
        return findTbEquipoEntities(false, maxResults, firstResult);
    }

    private List<TbEquipo> findTbEquipoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TbEquipo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TbEquipo findTbEquipo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TbEquipo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTbEquipoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TbEquipo> rt = cq.from(TbEquipo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
