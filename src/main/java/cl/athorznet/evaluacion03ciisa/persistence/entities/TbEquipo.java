package cl.athorznet.evaluacion03ciisa.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jarqu
 */
@Entity
@Table(name = "tb_equipos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbEquipo.findAll", query = "SELECT t FROM TbEquipo t"),
    @NamedQuery(name = "TbEquipo.findByIdRegistro", query = "SELECT t FROM TbEquipo t WHERE t.idRegistro = :idRegistro"),
    @NamedQuery(name = "TbEquipo.findByCodSerieEquipo", query = "SELECT t FROM TbEquipo t WHERE t.codSerieEquipo = :codSerieEquipo"),
    @NamedQuery(name = "TbEquipo.findByNomMarca", query = "SELECT t FROM TbEquipo t WHERE t.nomMarca = :nomMarca"),
    @NamedQuery(name = "TbEquipo.findByNomModelo", query = "SELECT t FROM TbEquipo t WHERE t.nomModelo = :nomModelo"),
    @NamedQuery(name = "TbEquipo.findByNomPropietario", query = "SELECT t FROM TbEquipo t WHERE t.nomPropietario = :nomPropietario"),
    @NamedQuery(name = "TbEquipo.findByApe1Propietario", query = "SELECT t FROM TbEquipo t WHERE t.ape1Propietario = :ape1Propietario"),
    @NamedQuery(name = "TbEquipo.findByApe2Propietario", query = "SELECT t FROM TbEquipo t WHERE t.ape2Propietario = :ape2Propietario"),
    @NamedQuery(name = "TbEquipo.findByFechaAlta", query = "SELECT t FROM TbEquipo t WHERE t.fechaAlta = :fechaAlta")})
public class TbEquipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Size(max = 2147483647)
    @Column(name = "cod_serie_equipo")
    private String codSerieEquipo;
    @Size(max = 2147483647)
    @Column(name = "nom_marca")
    private String nomMarca;
    @Size(max = 2147483647)
    @Column(name = "nom_modelo")
    private String nomModelo;
    @Size(max = 2147483647)
    @Column(name = "nom_propietario")
    private String nomPropietario;
    @Size(max = 2147483647)
    @Column(name = "ape1_propietario")
    private String ape1Propietario;
    @Size(max = 2147483647)
    @Column(name = "ape2_propietario")
    private String ape2Propietario;
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.DATE)
    private Date fechaAlta;

    public TbEquipo() {
    }

    public TbEquipo(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getCodSerieEquipo() {
        return codSerieEquipo;
    }

    public void setCodSerieEquipo(String codSerieEquipo) {
        this.codSerieEquipo = codSerieEquipo;
    }

    public String getNomMarca() {
        return nomMarca;
    }

    public void setNomMarca(String nomMarca) {
        this.nomMarca = nomMarca;
    }

    public String getNomModelo() {
        return nomModelo;
    }

    public void setNomModelo(String nomModelo) {
        this.nomModelo = nomModelo;
    }

    public String getNomPropietario() {
        return nomPropietario;
    }

    public void setNomPropietario(String nomPropietario) {
        this.nomPropietario = nomPropietario;
    }

    public String getApe1Propietario() {
        return ape1Propietario;
    }

    public void setApe1Propietario(String ape1Propietario) {
        this.ape1Propietario = ape1Propietario;
    }

    public String getApe2Propietario() {
        return ape2Propietario;
    }

    public void setApe2Propietario(String ape2Propietario) {
        this.ape2Propietario = ape2Propietario;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbEquipo)) {
            return false;
        }
        TbEquipo other = (TbEquipo) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.athorznet.evaluacion03ciisa.persistence.entities.TbEquipo[ idRegistro=" + idRegistro + " ]";
    }
    
}
