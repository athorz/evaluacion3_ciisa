<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="./bootstrap-4.4.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="./css/estilos.css" rel="stylesheet" id="bootstrap-css">
        <script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 3 ::Jorge Arqueros::</title>
    </head>
    <body>
        <div class="wrapper">

            <header>
                <h1>Evaluación 3</h1>
                <h3>Taller de Aplicaciones Empresariales</h3>
            </header>

            <section class="columns">

                <div class="column">
                    <h2>Métodos <b>GET</b></h2>
                    <p>Consulta todos los registros en la tabla de la Base de Datos</p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/</a></p>
                    <p>Consulta un registro <b>por ID (PK)</b> en la tabla de la Base de Datos</p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/33">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/{id}</a></p>
                </div>

                <div class="column">
                    <h2>Métodos <b>POST</b></h2>
                    <p>Inserta un nuevo elemento en la Base de Datos</p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/</a></p>

                </div>

                <div class="column">
                    <h2>Métodos <b>PUT</b></h2> 
                    <p>Actualiza/Modifica un elemento en la Base de Datos <b>ingresando la Id (PK) en la URI</b></p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/{id}">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/{id}</a></p>
                    <p>Actualiza/Modifica un elemento en la Base de Datos <b>ingresando la Id (PK) en el body JSON</b></p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/</a></p>
                </div>

                <div class="column">
                    <h2>Métodos <b>DELETE</b></h2>
                    <p>Elimina un elemento en la Base de Datos <b>ingresando una Id (PK)</b> existente</p>
                    <p><a target="_blank" href ="http://evaluacion03-jarqueros.herokuapp.com/api/equipos/{id}">http://evaluacion03-jarqueros.herokuapp.com/api/equipos/{id}</a></p>
                </div>
            </section>	

            <footer>
                <p>Desarrollado por Jorge Arqueros Reyes | Sección 50 | CIISA 03/05/2020</p>
            </footer>

        </div>
    </body>
</html>
